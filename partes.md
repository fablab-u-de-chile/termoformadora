# Partes principales

<img src="/imgs/d1.png" width="400">

| Item  | Parte |
| -------- | ------- |
| 1 | Chasis de perfiles T-Slot 2020   |
| 2 | Paneles de acrílico    |
| 3    | Panel de interfaz   |
| 4    | Panel de calefactores   |
| 5  | Marco   |

El chasis se construye de perfiles de aluminio T-Slot y sus accesorios. Este chasis está recubierto de paneles de acrílico. La parte superior cuenta con un ventilador para evacuar el calentamiento del aire por efecto de los calefactores ubicados en el panel de aluminio. El volumen inferior contiene a la electrónica. La parte frontal soporta la interfaz que está en ángulo para un uso más cómodo. 

El material se monta en un marco de aluminio que se desplaza y fija mediante un sistema de ejes, rodamientos y resortes.

<img src="/imgs/iso2.png" width="400">

La parte posterior de la máquina cuenta con la conexión eléctrica, el botón de encendido principal y la unión el tubo de aspiración.

<img src="/imgs/iso3.png" width="400">

Abajo del marco se encuentra el panel con orificios para disponer el molde.

<img src="/imgs/iso5.png" width="400">

Los calefactores son cerámicos, de 250 W, con termocupla incluida. 

<img src="/imgs/iso6.png" width="400">
