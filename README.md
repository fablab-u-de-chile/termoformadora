# Termoformadora

Una termoformadora de formato carta (22x28 mm), fabricada principalmente con acrílico y aluminio. Equipada con 3 calefactores cerámicos de 250 W, interfaz sencilla y con conexión a aspiradora.

<img src="/imgs/iso1.png" width="400">

La descripción de las partes principales de la máquina se encuentra [AQUÍ](partes.md).

Las instrucciones de uso se encuentran [AQUÍ](instrucciones.md).

El listado de partes, comerciales y fabricadas se encuentra [AQUÍ](https://docs.google.com/spreadsheets/d/11OmDvkU0BH5-Cy3GA1U_RXPpzjDjjaHvGAx2V0GPiX8/edit?usp=sharing).

Los procesos para la fabricación de esta máquina fueron impresión 3D y corte CNC. Las partes impresas son de PLA o PETG. Las cubiertas de acrílico (8 mm) fueron cortadas con CNC, así como el panel de aluminio 1100 (3 mm). Los perfiles de aluminio fueron cortados con sierra manual y rectificados con CNC.

Hasta ahora se han hecho pruebas con PAI de 0.5 hasta 1.2 mm de espesor sobre moldes impresos en 3D, como el que se observa en la imagen más abajo.

<img src="/imgs/t1.png" width="500">

## Trabajo futuro

Completar la documentación, incluuir diagramas eléctricos.


