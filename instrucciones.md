# Instrucciones de uso

Antes de hacer el proceso de termoformado, es necesario contar con una pieza para servir de molde (con ciertas consideraciones de diseño) y una lámina de termoplástico cortada en formato carta.

1. Levantar el marco hasta la mitad de la carrera de los ejes. Esto se hace jalando hacia afuera las manilla para soltar el perno de retención, luego se levanta el marco hasta la altura deseada y se deja de jalar las manillas.
2. Remover las tuercas mariposa y quitar el marco superior.
3. Montar el material en el maroc inferior.
4. Volver a montar el marco superior.
5. Encender la máquina mediante el interruptor ubicado en la parte posterior.
6. Conectar el tubo de la aspiradora en la máquina.
7. Seleccionar una temperatura adecuada para el material a utilizar (generalmente entre 200 y 300 C). Apretar el botón verde.
8. Esperar a que la temperatura se estabilice.
9. Levantar el marco hasta que el material esté a una distancia de entre 3 a 4 cm respecto a los calefactores.
10. Ubicar la pieza en su lugar en el panel con orificios.
11. Esperar a que el material se ablande, debiese observarse una depresión en su superficie.
12. Encender la aspiradora.
13. Bajar el marco firmemente hasta el tope inferior.
14. Apagar la aspiradora.
15. Apagar los calefactores pulsando el botón rojo.
16. Remover el marco superior y quitar el material termoformado.



